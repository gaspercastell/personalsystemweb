package santander.com.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import santander.com.service.ExcelService;


@Controller
@RequestMapping("/personal")
public class PersonaController {
	
	public static final Logger LOG = LoggerFactory.getLogger(PersonaController.class);
	
	@Autowired
	ExcelService excelService;
	
	@PostMapping("/ReadCSV")
	public String ReadCSV(@RequestParam("file") MultipartFile file, RedirectAttributes attributes) {
		LOG.info("--- Leyendo excel ");
		
        if (file.isEmpty()) {
        	attributes.addFlashAttribute("error", "Por favor seleccione un archivo.");
            return "redirect:/";
        }else {
        	if (excelService.esFormatoCorrecto(file)) {
        		try {
        			excelService.procesarExcel(file);
        			attributes.addFlashAttribute("message", "Se cargo correctamente.");
  		      } catch (Exception e) {
  		    	LOG.error("--- Error: "+e.getMessage());
  		    	attributes.addFlashAttribute("error", "Hubo un error al procesar el archivo Error: "+e.getMessage());
  		      }
        	}else {
        		attributes.addFlashAttribute("error", "El archivo no es valido");
  		    }
        }
        return "redirect:/";
	}
}
