package santander.com.service;

import java.util.Date;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import santander.com.controller.PersonaController;
import santander.com.model.PersonaDto;
import santander.com.repository.PersonaRepository;

@Service
public class ExcelServiceImpl implements ExcelService{
	@Value("${spring.mvc.contentnegotiation.media-types.xlsx}")
    private String TYPE;
	
	public static final Logger LOG = LoggerFactory.getLogger(PersonaController.class);
	
	@Autowired
	private PersonaRepository repository;

	@Override
	public boolean esFormatoCorrecto(MultipartFile file) {
	    if (!TYPE.equals(file.getContentType())) {
	        return false;
	      }
	    return true;
	}

	@Override
	public void procesarExcel(MultipartFile file) throws Exception{

		XSSFWorkbook libro = new XSSFWorkbook(file.getInputStream());
		XSSFSheet hoja = libro.getSheetAt(0);
		for(int i=0; i<hoja.getPhysicalNumberOfRows();i++) {
			XSSFRow fila = hoja.getRow(i);
			if((fila.getPhysicalNumberOfCells()-1)>0) {
				PersonaDto persona = new PersonaDto();
				persona.setId((int)fila.getCell(0).getNumericCellValue());
				persona.setNombre(fila.getCell(1).getStringCellValue());
				persona.setApellido(fila.getCell(2).getStringCellValue());
				persona.setEdad((int)(fila.getCell(3).getNumericCellValue()));
				persona.setCalle(fila.getCell(4).getStringCellValue());
				persona.setCiudad(fila.getCell(5).getStringCellValue());
				persona.setEstado(fila.getCell(6).getStringCellValue());
				persona.setCp((int)fila.getCell(7).getNumericCellValue());
				persona.setSueldo(fila.getCell(8).getNumericCellValue());
				persona.setColor(fila.getCell(9).getStringCellValue());
				persona.setFecha_nacimiento(fila.getCell(10).getDateCellValue());
				repository.save(persona);
				imprimeInformación();
			}
		}
		libro.close();
	}
	
	private void imprimeInformación() {
		List<PersonaDto> personal = repository.findAll();
		for(PersonaDto per:personal) {
			System.out.println("=============");
			System.out.println(per.getNombre());
		}
	}

}
