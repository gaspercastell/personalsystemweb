package santander.com.service;

import org.springframework.web.multipart.MultipartFile;

public interface ExcelService {

	public boolean esFormatoCorrecto(MultipartFile file);
	public void procesarExcel(MultipartFile file)throws Exception;
	
}
