package santander.com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import santander.com.model.PersonaDto;

@Repository
public interface PersonaRepository extends JpaRepository<PersonaDto,Integer>{
	
	
}
